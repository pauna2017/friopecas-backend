define({ "api": [
  {
    "type": "post",
    "url": "/login",
    "title": "Login",
    "name": "Login",
    "group": "Autenticacao",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "email",
            "optional": false,
            "field": "Email",
            "description": "<p>Email of the user, this is unique.</p>"
          },
          {
            "group": "Parameter",
            "type": "password",
            "optional": false,
            "field": "Password",
            "description": "<p>Password of the user.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"token\": \"JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTE4OGY3ODUzMzViZDFjNTI2ZmRmYjEiLCJmaXJzdE5hbWUiOiJtYXRldXMiLCJsYXN0TmFtZSI6ImR1cmFlcyIsImVtYWlsIjoiZHVyYWVzQGdtYWlsLmNvbSIsInJvbGUiOiJpbnN0YWxsZXIiLCJpYXQiOjE0OTQ5ODI1NzQsImV4cCI6MTQ5NDk5MjY1NH0.nlW7k8dZlRAW52Bxa_RrmuVPR-O_tI6DVKo0mXybsi8\",\n  \"installer\": {\n     \"_id\": \"59188f785335bd1c526fdfb2\",\n     \"user\": {\n       \"_id\": \"59188f785335bd1c526fdfb1\",\n       \"updatedAt\": \"2017-05-14T17:10:16.065Z\",\n       \"createdAt\": \"2017-05-14T17:10:16.065Z\",\n       \"email\": \"duraes@gmail.com\",\n       \"firstName\": \"mateus\",\n       \"lastName\": \"duraes\",\n       \"__v\": 0,\n       \"role\": \"installer\"\n     },\n   \"rating\": \"5\",\n   \"zipCode\": \"31530250\",\n   \"photo\": \"base64here\"\n   \"__v\": \"0\"\n   }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "Unauthorized",
            "description": "<ul> <li>Invalid credentials</li> </ul>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 401 Unauthorized\nUnauthorized",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/users/controller.js",
    "groupTitle": "Autenticacao"
  },
  {
    "type": "delete",
    "url": "/installer/:id",
    "title": "Delete Installer",
    "group": "Instaladores",
    "description": "<p>Delete a installer in database.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 204 No-Content",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "BadRequest",
            "description": "<ul> <li>Invalid ObjectId</li> </ul>"
          }
        ],
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "NotFound",
            "description": "<ul> <li>Installer not found</li> </ul>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<ul> <li>InternalServerError</li> </ul>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/installers/controller.js",
    "groupTitle": "Instaladores",
    "name": "DeleteInstallerId"
  },
  {
    "type": "get",
    "url": "/installer/:id",
    "title": "Get Specific Installer",
    "group": "Instaladores",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Users unique ID.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "User",
            "description": "<p>data Object containing email and name.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "Photo",
            "description": "<p>Photo of Installer.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "ZipCode",
            "description": "<p>Zipcode of Installer.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "Firstname",
            "description": "<p>Firstname of the User.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "BadRequest",
            "description": "<ul> <li>Invalid ObjectId</li> </ul>"
          }
        ],
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "UserNotFound",
            "description": "<ul> <li>Installer not found.</li> </ul>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/installers/controller.js",
    "groupTitle": "Instaladores",
    "name": "GetInstallerId"
  },
  {
    "type": "get",
    "url": "/installers",
    "title": "Get All Installers",
    "group": "Instaladores",
    "success": {
      "fields": {
        "200": [
          {
            "group": "200",
            "type": "Object",
            "optional": false,
            "field": "User",
            "description": "<p>data Object containing email and name.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "Photo",
            "description": "<p>Photo of Installer.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "ZipCode",
            "description": "<p>Zipcode of Installer.</p>"
          },
          {
            "group": "200",
            "type": "String",
            "optional": false,
            "field": "Firstname",
            "description": "<p>Firstname of the User.</p>"
          }
        ]
      }
    },
    "error": {
      "examples": [
        {
          "title": "Error-Response:",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "api/installers/controller.js",
    "groupTitle": "Instaladores",
    "name": "GetInstallers"
  },
  {
    "type": "post",
    "url": "/installer/",
    "title": "Register Installer",
    "group": "Instaladores",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstName",
            "description": "<p>Firstname of installer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastName",
            "description": "<p>Lastname of installer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of installer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": "<p>Need to be 'installer'.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "rating",
            "description": "<p>Start rating of installer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "zipCode",
            "description": "<p>Zipcode of installer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photo",
            "description": "<p>Base64 encoded photo of installer.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n \"user\": {\n      \"_id\": \"591ba50e19af5b460a809732\",\n      \"firstName\": \"mateus\",\n      \"lastName\": \"duraes\",\n      \"email\": \"duraes123@gmail.com\",\n      \"role\": \"installer\"\n  },\n  \"token\": \"JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTFiYTUwZTE5YWY1YjQ2MGE4MDk3MzIiLCJmaXJzdE5hbWUiOiJtYXRldXMiLCJsYXN0TmFtZSI6ImR1cmFlcyIsImVtYWlsIjoiZHVyYWVzMTIzQGdtYWlsLmNvbSIsInJvbGUiOiJpbnN0YWxsZXIiLCJpYXQiOjE0OTQ5ODM5NTAsImV4cCI6MTQ5NDk5NDAzMH0.X2syINhC5MQlxVyE0ghWfPUHF0ebbAlWXwhXmYahCd8\",\n  \"installer\": {\n      \"__v\": 0,\n      \"user\": \"591ba50e19af5b460a809732\",\n      \"rating\": 5,\n      \"zipCode\": \"31530250\",\n      \"photo\": \"photobase64\",\n      \"_id\": \"591ba50e19af5b460a809733\"\n  }\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "BadRequest",
            "description": "<ul> <li>Missing fields</li> </ul>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<ul> <li>InternalServerError</li> </ul>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/installers/controller.js",
    "groupTitle": "Instaladores",
    "name": "PostInstaller"
  },
  {
    "type": "put",
    "url": "/installer/:id",
    "title": "Update Installer",
    "group": "Instaladores",
    "description": "<p>You can send only the data to be updated.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of user.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstName",
            "description": "<p>Firstname of installer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastName",
            "description": "<p>Lastname of installer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>Password of installer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "role",
            "description": "<p>Need to be 'installer'.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "rating",
            "description": "<p>Start rating of installer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "zipCode",
            "description": "<p>Zipcode of installer.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "photo",
            "description": "<p>Base64 encoded photo of installer.</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "    HTTP/1.1 200 OK\n{\n  \"_id\": \"59188f785335bd1c526fdfb2\",\n  \"user\": {\n    \"_id\": \"59188f785335bd1c526fdfb1\",\n    \"updatedAt\": \"2017-05-14T17:10:16.065Z\",\n    \"createdAt\": \"2017-05-14T17:10:16.065Z\",\n    \"email\": \"duraes@gmail.com\",\n    \"firstName\": \"mateus\",\n    \"lastName\": \"duraes\",\n    \"__v\": 0,\n    \"role\": \"installer\"\n  },\n  \"rating\": 5,\n  \"zipCode\": \"31530250\",\n  \"photo\": \"photobase64444444444444444\",\n  \"__v\": 0\n  }",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "400": [
          {
            "group": "400",
            "optional": false,
            "field": "BadRequest",
            "description": "<ul> <li>Bad request</li> </ul>"
          }
        ],
        "404": [
          {
            "group": "404",
            "optional": false,
            "field": "NotFound",
            "description": "<ul> <li>Installer not found</li> </ul>"
          }
        ],
        "500": [
          {
            "group": "500",
            "optional": false,
            "field": "InternalServerError",
            "description": "<ul> <li>InternalServerError</li> </ul>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "api/installers/controller.js",
    "groupTitle": "Instaladores",
    "name": "PutInstallerId"
  }
] });
