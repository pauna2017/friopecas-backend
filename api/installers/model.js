const mongoose = require('mongoose')

const Schema = mongoose.Schema
const InstallerSchema = new Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    photo: {type: String, required: true},
    rating: {type: Number, required: true, default: 5},
    zipCode: {type: String, required: true},
    city: {type: String, required: true},
    state: {type: String, required: true},
    codigoFp: {type: Number, required: true, index: {unique: true}},
})
module.exports = mongoose.model('Installer', InstallerSchema)
