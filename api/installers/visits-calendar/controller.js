const Visit = require('./model')
const operations = require('./operations')
const Auth = require('../../users/auth')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {session: false})
const roles = require('../../../config/roles')

module.exports = app => {  

    app.get('/installer/visits', requireAuth, Auth.roleAuthorization(roles.REQUIRE_ADMIN), operations.getAllVisits)


    app.get('/installer/:idInstaller/visits', 
        requireAuth,
        Auth.roleAuthorization(roles.REQUIRE_INSTALLER),
        operations.getAllInstallerVisits
    )

    app.post('/installer/:id/visits',
        requireAuth,
        Auth.roleAuthorization(roles.REQUIRE_INSTALLER),
        operations.registerVisit
    )    
    
    // app.put('/installer/:id/visits')
    // app.delete('/installer/:id/visits')

}
