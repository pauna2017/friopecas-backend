const mongoose = require('mongoose')

const Schema = mongoose.Schema
const VisitSchema = new Schema({
    installer: {type: mongoose.Schema.Types.ObjectId, ref: 'Installer', required: true},
    date: {type: Date, required: true},
    address: {
        street: {type: String},
        zipCode: {type: String},
        streetNumber: {type: String}
    },
    
    clientName: {type: String},
    description: {type: String},
    status: {type: Boolean, default: true}
})
module.exports = mongoose.model('Visit', VisitSchema)
