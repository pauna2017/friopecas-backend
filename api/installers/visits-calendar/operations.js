const Installer = require('../model')
const Visit = require('./model')
const mongoose = require('mongoose')


const getAllVisits = (req, res) => {   
    Visit.find()
        .then(doc => res.status(200).send(doc))
        .catch(err => res.status(500).send(err))
}

const getAllInstallerVisits = (req, res) => {
    const installerId = req.params.idInstaller
    Visit.find({installer: installerId})
        .sort({date : 'desc'})        
        .then(doc => res.status(200).json(doc))
        .catch(err => res.status(400).json(err))
}

const registerVisit = (req, res) => {

    const visitToSave = new Visit(req.body)
    visitToSave.save()
        .then(doc =>res.status(201).json(doc))
        .catch(err => res.status(400).json(err))
}

const updateVisit  = (req, res)=> {       
}

const deleteVisit = (req, res) => {          
}


exports.getAllVisits = getAllVisits
exports.getAllInstallerVisits = getAllInstallerVisits
exports.registerVisit = registerVisit
exports.updateVisit = updateVisit
exports.deleteVisit = deleteVisit

