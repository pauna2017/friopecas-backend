const Installer = require('./model')
const operations = require('./operations')
const Auth = require('../users/auth')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {session: false})
const roles = require('../../config/roles')

module.exports = app => {  

    /**
     * @api {get} /installers Get All Installers
     * @apiGroup Instaladores
     * @apiSuccess (200) {Object} User data Object containing email and name.
     * @apiSuccess (200) {String} Photo Photo of Installer.
     * @apiSuccess (200) {String} ZipCode Zipcode of Installer.
     * @apiSuccess (200) {String} Firstname Firstname of the User.
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 500 Internal Server Error
     */
    
    app.get('/installer', operations.getAllInstallers)


    /**
     * @api {get} /installer/:id Get Specific Installer
     * @apiGroup Instaladores
     * @apiParam {String} id Users unique ID.
     * 
     * @apiSuccess (200) {Object} User data Object containing email and name.
     * @apiSuccess (200) {String} Photo Photo of Installer.
     * @apiSuccess (200) {String} ZipCode Zipcode of Installer.
     * @apiSuccess (200) {String} Firstname Firstname of the User.
     *
     * @apiError (404) UserNotFound -  Installer not found.
     * @apiError (400) BadRequest -  Invalid ObjectId
     */
    app.get('/installer/:id', requireAuth, operations.getSpecificInstaller)



    /**
     * @api {post} /installer/ Register Installer
     * @apiGroup Instaladores
     * 
     * @apiParam {String} email Email of user.
     * @apiParam {String} firstName Firstname of installer.
     * @apiParam {String} lastName Lastname of installer.
     * @apiParam {String} password Password of installer.
     * @apiParam {String} role Need to be 'installer'.
     * @apiParam {String} rating Start rating of installer.
     * @apiParam {String} zipCode Zipcode of installer.
     * @apiParam {String} photo Base64 encoded photo of installer.
     * 
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     * {
     *  "user": {
     *       "_id": "591ba50e19af5b460a809732",
     *       "firstName": "mateus",
     *       "lastName": "duraes",
     *       "email": "duraes123@gmail.com",
     *       "role": "installer"
     *   },
     *   "token": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTFiYTUwZTE5YWY1YjQ2MGE4MDk3MzIiLCJmaXJzdE5hbWUiOiJtYXRldXMiLCJsYXN0TmFtZSI6ImR1cmFlcyIsImVtYWlsIjoiZHVyYWVzMTIzQGdtYWlsLmNvbSIsInJvbGUiOiJpbnN0YWxsZXIiLCJpYXQiOjE0OTQ5ODM5NTAsImV4cCI6MTQ5NDk5NDAzMH0.X2syINhC5MQlxVyE0ghWfPUHF0ebbAlWXwhXmYahCd8",
     *   "installer": {
     *       "__v": 0,
     *       "user": "591ba50e19af5b460a809732",
     *       "rating": 5,
     *       "zipCode": "31530250",
     *       "photo": "photobase64",
     *       "_id": "591ba50e19af5b460a809733"
     *   }
     * }
     *      
     * @apiError (400) BadRequest -  Missing fields
     * @apiError (500) InternalServerError -  InternalServerError
     */
    app.post('/installer', Auth.register, operations.registerInstaller)


    /**
     * @api {put} /installer/:id Update Installer
     * @apiGroup Instaladores
     * 
     * @apiDescription You can send only the data to be updated.
     * 
     * @apiParam {String} email Email of user.
     * @apiParam {String} firstName Firstname of installer.
     * @apiParam {String} lastName Lastname of installer.
     * @apiParam {String} password Password of installer.
     * @apiParam {String} role Need to be 'installer'.
     * @apiParam {String} rating Start rating of installer.
     * @apiParam {String} zipCode Zipcode of installer.
     * @apiParam {String} photo Base64 encoded photo of installer.
     * 
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     * {
     *   "_id": "59188f785335bd1c526fdfb2",
     *   "user": {
     *     "_id": "59188f785335bd1c526fdfb1",
     *     "updatedAt": "2017-05-14T17:10:16.065Z",
     *     "createdAt": "2017-05-14T17:10:16.065Z",
     *     "email": "duraes@gmail.com",
     *     "firstName": "mateus",
     *     "lastName": "duraes",
     *     "__v": 0,
     *     "role": "installer"
     *   },
     *   "rating": 5,
     *   "zipCode": "31530250",
     *   "photo": "photobase64444444444444444",
     *   "__v": 0
     *   }
     *      
     * @apiError (400) BadRequest -  Bad request
     * @apiError (404) NotFound -  Installer not found
     * @apiError (500) InternalServerError -  InternalServerError
     */
    app.put('/installer/:id', requireAuth, Auth.roleAuthorization(roles.REQUIRE_ADMIN), operations.updateInstaller)



    /**
     * @api {delete} /installer/:id Delete Installer
     * @apiGroup Instaladores
     * @apiDescription Delete a installer in database.
     * @apiParam {String} id Users unique ID.
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 204 No-Content
     * @apiError (400) BadRequest -  Invalid ObjectId
     * @apiError (404) NotFound -  Installer not found
     * @apiError (500) InternalServerError -  InternalServerError
     */
    app.delete('/installer/:id', requireAuth, Auth.roleAuthorization(roles.REQUIRE_ADMIN) ,operations.deleteInstaller)

}
