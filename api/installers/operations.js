const User = require('../users/model')
const Installer = require('./model')
const aws = require('aws-sdk')
const S3_BUCKET = process.env.S3_BUCKET
const sharp = require('sharp')

const getAllInstallers = (req, res) => {   
    Installer.find()
        .populate('user', '-password')
        .then(doc => res.status(200).send(doc))
        .catch(err => res.status(500).send(err))
}

const getSpecificInstaller = (req, res) => {
    const id = req.params.id
    Installer.findById(id)
        .populate('user', '-password')
        .then(installer => {
            if (installer)  {
                let foundedInstaller = installer.toObject()
                delete foundedInstaller.password
                res.status(200).json(installer)
            }
            else res.status(404).json({message: 'Installer not found.'})
        })
        .catch(err => res.status(400).json(err))
}

const registerInstaller = (req, res) => {
    const userCreated = req.user;
    const installer = new Installer(req.body)
    installer.user = userCreated.user._id
    if (installer.photo) {
            sharp(new Buffer(installer.photo, 'base64'))
            .resize(200, 200)
            .toBuffer((err, info) => {
                if(!err){
                    const buf = new Buffer(info, 'base64')
                    
                    const s3Bucket = new aws.S3({params: {Bucket: 'friopecass3'}, signatureVersion: 'v4'})
                    const data = {
                        Key: `${installer.user}_profile.jpg`,
                        Body: buf,
                        ContentEncoding: 'base64',
                        ContentType: 'image/jpeg'
                    };
                    s3Bucket.putObject(data, (err, image) => {
                        Promise.resolve(err)
                            .then(err => {
                                if(err) {
                                    throw(err)
                                }
                            })
                            .then(url => {
                                installer.photo = `https://s3.us-east-2.amazonaws.com/friopecass3/${installer.user}_profile.jpg`
                                return installer.save()
                            })
                            .then(doc => {
                                const installerCreated = doc.toObject()
                                delete installerCreated.password
                                userCreated.installer = installerCreated            
                                userCreated.installer.user = userCreated.user
                                res.status(201).json(userCreated)
                            })
                            .catch(err => {
                                console.log('err', err);
                                User.findOneAndRemove({_id: userCreated.user._id})
                                    .then(doc => res.status(400).json(err))
                                    .catch(() => res.status(500).json(err))
                            })  
            })
        }})
    } else {
        console.log('Registering client without photo')
        Promise.resolve(`https://s3.us-east-2.amazonaws.com/friopecass3/default_profile.png`)
            .then(url => {
                installer.photo = url
                return installer.save()
            })
            .then(doc => {
                const installerCreated = doc.toObject()
                delete installerCreated.password
                userCreated.installer = installerCreated  
                userCreated.installer.user = userCreated.user               
                res.status(201).json(userCreated)
            })
            .catch(err => {
                console.log('err', err);
                User.findOneAndRemove({_id: userCreated.user._id})
                    .then(doc => res.status(400).json(err))
                    .catch(() => res.status(500).json(err))
            })  
    }            
}

const updateInstaller  = (req, res)=> {
    const id = req.params.id
    Installer.findOneAndUpdate(
        {_id: id}, req.body, {new: true})
        .populate('user', '-password')
        .then(installer => {
            if(installer) res.status(200).json(installer)
            else res.status(404).json({message: 'Installer not found.'})
        })
        .catch(err => res.status(400).json(err))
        
}

const deleteInstaller = (req, res) => {
    const id = req.params.id
    console.log(id)
    Installer.findOneAndRemove({_id: id})
        .then(doc => {
            if (doc) res.status(204).send()
            else res.status(404).json({message: 'Installer not found.'})                
        })
        .catch(err => res.status(400).send(err))            
}


exports.getAllInstallers = getAllInstallers
exports.getSpecificInstaller = getSpecificInstaller
exports.registerInstaller = registerInstaller
exports.updateInstaller = updateInstaller
exports.deleteInstaller = deleteInstaller
