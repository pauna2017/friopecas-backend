const Chat = require('./model')
const operations = require('./operations')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {session: false})
const roles = require('../../config/roles')

module.exports = app => {  

    app.get('/chat', requireAuth, operations.getAllChats)
    app.get('/chat/:id', requireAuth, operations.getSpecificChat)
    app.post('/chat', requireAuth, operations.initializeChat)
    app.put('/chat/:id', requireAuth, operations.updateChat)

    // app.get('user/:id/chat/', requireAuth, operations.getSpecificChat)
    // app.get('user/:id/chat/:id', requireAuth, operations.getSpecificChat)
}

