const getAllChats = (req, res) => {
    Chat.find()
        .then(doc => res.status(200).send(doc))
        .catch(err => res.status(500).send(err))
}

const getSpecificChat = (req, res) => {
    const id = req.params.id
    Chat.findById(id)
        .then(chat => {
            if (chat) res.status(200).json(chat)
            else res.status(404).json({message: 'Chat not found.'})
        })
        .catch(err => res.status(400).json(err))
}

const initializeChat = (req, res) => {
    let chat = new Chat(req.body)              
    chat.save()            
        .then(doc => {  
            res.status(201).json(doc)
        })
        .catch(err => res.status(400).send(err))
}

const updateChat = (req, res) => {
    const id = req.params.id
    Chat.findById(id)
        .then(chat => {
            if(!chat) res.status(404).json({message: 'Chat not found'})
            else {
                console.log('req body', req.body)
                chat.body.push(req.body)
                chat.save()
                    .then(doc => {
                        console.log(doc)
                        res.status(200).send(chat)
                    })
                    .catch(err => res.status(404).json(err))
            }
        })
        .catch(err => res.status(400).json(err))
}

exports.getAllChats = getAllChats
exports.getSpecificChat = getSpecificChat
exports.initializeChat = initializeChat
exports.updateChat = updateChat