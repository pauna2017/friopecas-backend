const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ChatSchema = new Schema({
    idClient:  {type: mongoose.Schema.Types.ObjectId, ref: 'Client', required: true},
    idInstaller:  {type: mongoose.Schema.Types.ObjectId, ref: 'Installer', required: true},
    body: [{
        message: {type: String, require: true}, 
        date: {type: Date, default: Date.now},
        author: {type: mongoose.Schema.Types.ObjectId, required: true}
    }]
})
module.exports = mongoose.model('Chat', ChatSchema)
