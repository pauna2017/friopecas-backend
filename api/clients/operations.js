const User = require('../users/model')
const Client = require('./model')
const aws = require('aws-sdk')
const S3_BUCKET = process.env.S3_BUCKET
const sharp = require('sharp')


const getAllClient = (req, res) => {
    Client.find()
        .then(doc => res.status(200).send(doc))
        .catch(err => res.status(500).send(err))
}

const getSpecificClient = (req, res) => {
    const id = req.params.id
    Client.findById(id)
        .then(client => {
            if (client)  {
                let foundedClient = client.toObject()
                delete foundedClient.password
                res.status(200).json(foundedClient)
            }
            else res.status(404).json({message: 'Client not found.'})
        })
        .catch(err => res.status(400).json(err))
}

const registerClient = (req, res) => {
    const userCreated = req.user;
    console.log(userCreated)
    const client = new Client(req.body);
    client.user = userCreated.user._id;

    if (client.photo) {
        console.log('Registering client with photo')
        sharp(new Buffer(client.photo, 'base64'))
        .resize(200, 200)
        .toBuffer((err, info) => {
            if(!err){
                const buf = new Buffer(info, 'base64')
                
                const s3Bucket = new aws.S3({params: {Bucket: 'friopecass3'}, signatureVersion: 'v4'})
                const data = {
                    Key: `${client.user}_profile.jpg`,
                    Body: buf,
                    ContentEncoding: 'base64',
                    ContentType: 'image/jpeg'
                };
                s3Bucket.putObject(data, (err, image) => {
                    Promise.resolve(err)
                        .then(err => {
                            if(err) {
                                throw(err)
                            }
                        })
                        .then(() => {
                            client.photo = `https://s3.us-east-2.amazonaws.com/friopecass3/${client.user}_profile.jpg`
                            return client.save()
                        })
                        .then(doc => {
                            const clientCreated = doc.toObject()
                            delete clientCreated.password
                            userCreated.client = clientCreated            
                            res.status(201).json(userCreated)
                        })
                        .catch(err => {
                            console.log('err', err);
                            User.findOneAndRemove({_id: userCreated.user._id})
                                .then(doc => res.status(400).json(err))
                                .catch(() => res.status(500).json(err))
                        })        
                })
            }
        });
    } else {
        console.log('Registering client without photo')
        Promise.resolve(`https://s3.us-east-2.amazonaws.com/friopecass3/default_profile.png`)
            .then((photoDefault) => {
                client.photo = photoDefault
                return client.save()
            })
            .then(doc => {
                const clientCreated = doc.toObject()
                delete clientCreated.password
                userCreated.client = clientCreated            
                res.status(201).json(userCreated)
            })
            .catch(err => {
                console.log('err', err);
                User.findOneAndRemove({_id: userCreated.user._id})
                    .then(doc => res.status(400).json(err))
                    .catch(() => res.status(500).json(err))
            }) 
    }

    

}

// Forte candidato a virar umm módulo
const sendPhoto = (photo, userId) => {
    if(photo){
        return sharp(new Buffer(photo, 'base64'))
        .resize(200, 200)
        .toBuffer((err, info) => {
            if(!err){
                const buf = new Buffer(info, 'base64')
                
                const s3Bucket = new aws.S3({params: {Bucket: 'friopecass3'}, signatureVersion: 'v4'})
                const data = {
                    Key: `${userId}_profile.jpg`,
                    Body: buf,
                    ContentEncoding: 'base64',
                    ContentType: 'image/jpeg'
                };
                return s3Bucket.putObject(data).promise()
                    .then(data => `https://s3.us-east-2.amazonaws.com/friopecass3/${userId}_profile.jpg`)
                    .catch(err => err)
            } else {
                return Promise.reject(err);
            }
        })
    } else {
        const urlNoPhoto = `https://s3.us-east-2.amazonaws.com/friopecass3/default_profile.png`
        return Promise.resolve(urlNoPhoto)
    }    
}

const updateClient = (req, res) => {
    const id = req.params.id
    Client.findOneAndUpdate(
        {_id: id}, req.body, {new: true})
        .then(client => {
            if(client) res.status(200).json(client)
            else res.status(404).json({message: 'Client not found.'})
        })
        .catch(err => res.status(400).json(err))
}


exports.getAllClient = getAllClient
exports.getSpecificClient = getSpecificClient
exports.registerClient = registerClient
exports.updateClient = updateClient