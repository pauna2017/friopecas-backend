const mongoose = require('mongoose')

const Schema = mongoose.Schema
const ClientSchema = new Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true},
    photo: {type: String, required: true},    
    street: {type: String, required: true},
    streetNumber: {type: String, required: true},
    city: {type: String, required: true},
    state: {type: String, required: true},
    district: {type: String, required: true},
    zipCode: {type: String, required: true}
})
module.exports = mongoose.model('Client', ClientSchema)
