const Client = require('./model')
const operations = require('./operations')
const Auth = require('../users/auth')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {session: false})
const roles = require('../../config/roles')

module.exports = app => {  

    app.get('/client', requireAuth, Auth.roleAuthorization(roles.REQUIRE_ADMIN), operations.getAllClient)
    app.get('/client/:id', requireAuth, Auth.roleAuthorization(roles.REQUIRE_CLIENT), operations.getSpecificClient)
    app.post('/client', Auth.register, operations.registerClient)
    app.put('/client/:id', requireAuth, Auth.roleAuthorization(roles.REQUIRE_CLIENT), operations.updateClient)
    
}
