const mongoose = require('mongoose')

const Schema = mongoose.Schema

const InstallersFrioPecasSchema = new Schema({
    codigo: {type: String, required: true},
    nome: {type: String, required: true},
    cidade: {type: String, required: true},
    estado: {type: String, required: true},
    numcep: {type: String, required: true}
})

module.exports =  mongoose.model('InstallersFrioPecas', InstallersFrioPecasSchema)