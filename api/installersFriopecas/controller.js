const Installer = require('./model')
const operations = require('./operations')
const Auth = require('../users/auth')
const passport = require('passport')
const requireAuth = passport.authenticate('jwt', {session: false})
const roles = require('../../config/roles')

module.exports = app => {  
    
    app.get('/installerfp', operations.getAllInstallersFp)

}
    