const User = require('../users/model')
const InstallerFp = require('./model')

const getAllInstallersFp = (req, res) => {
    InstallerFp.find(
        {
            nome: new RegExp(
            (req.query.name ? req.query.name : ''),
            'i'
         )
        }
    )
        .then(doc => res.status(200).json(doc))
        .catch(err => res.status(500).json(err))
}


exports.getAllInstallersFp = getAllInstallersFp
