const Installer = require('./model')
const Auth = require('./auth')
const passportService = require('../../config/passport')
const passport = require('passport')
const roles = require('../../config/roles')

const requireAuth = passport.authenticate('jwt', {session: false})
const requireLogin = passport.authenticate('local', {session: false})

module.exports = app => {  

    /**
     * @api {post} /login Login
     * @apiName Login
     * @apiGroup Autenticacao
     *
     * @apiParam {email} Email Email of the user, this is unique.
     * @apiParam {password} Password Password of the user.
     *
     * @apiSuccessExample Success-Response:
     *     HTTP/1.1 200 OK
     *     {
     *       "token": "JWT eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1OTE4OGY3ODUzMzViZDFjNTI2ZmRmYjEiLCJmaXJzdE5hbWUiOiJtYXRldXMiLCJsYXN0TmFtZSI6ImR1cmFlcyIsImVtYWlsIjoiZHVyYWVzQGdtYWlsLmNvbSIsInJvbGUiOiJpbnN0YWxsZXIiLCJpYXQiOjE0OTQ5ODI1NzQsImV4cCI6MTQ5NDk5MjY1NH0.nlW7k8dZlRAW52Bxa_RrmuVPR-O_tI6DVKo0mXybsi8",
     *       "installer": {
     *          "_id": "59188f785335bd1c526fdfb2",
     *          "user": {
     *            "_id": "59188f785335bd1c526fdfb1",
     *            "updatedAt": "2017-05-14T17:10:16.065Z",
     *            "createdAt": "2017-05-14T17:10:16.065Z",
     *            "email": "duraes@gmail.com",
     *            "firstName": "mateus",
     *            "lastName": "duraes",
     *            "__v": 0,
     *            "role": "installer"
     *          },
     *        "rating": "5",
     *        "zipCode": "31530250",
     *        "photo": "base64here"
     *        "__v": "0"
     *        }
     *     }
     *
     * @apiError Unauthorized - Invalid credentials
     *
     * @apiErrorExample Error-Response:
     *     HTTP/1.1 401 Unauthorized
     *     Unauthorized
     */
    app.post('/login', requireLogin, Auth.login)

    app.get('/teste', requireAuth, Auth.roleAuthorization(roles.REQUIRE_INSTALLER), (req, res, next) => {
      res.status(200).json({message: 'É nois na fita mano loko'})
    })


}
