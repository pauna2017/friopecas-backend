const User = require('./model')
const jwt = require('jsonwebtoken') 
const crypto = require('crypto')
const config = require('../../config/secret')
const model = {
  'client': require('../clients/model'),
  'installer': require('../installers/model')
}


const generateToken = user => jwt.sign(user, config.secret, {expiresIn: 10080 /* in seconds*/})
const setUserInfo = request => {  
  return {
    _id: request._id,
    firstName: request.firstName,
    lastName: request.lastName,
    email: request.email,
    role: request.role,
  }
}
//========================================
// Login Route
//========================================
exports.login = (req, res, next) => {
  const userInfo = setUserInfo(req.user)

  model[userInfo.role]
    .findOne({user: userInfo._id})
    .populate('user', '-password')
    .then(doc => {
      res.status(200).json({
        token: 'JWT ' + generateToken(userInfo),
        [userInfo.role]: doc
      })
    })
    .catch(err => {
      res.status(500).json(err)
    })
}

//=====================================================
// Authorization Middleware - Role authorization check
//=====================================================
exports.roleAuthorization = (role) => {  
  return (req, res, next) => {
    const user = req.user

    User.findById(user._id, (err, foundUser) => {
      if (err) {
        res.status(422).json({ error: 'No user was found.' })
        return next(err)
      }

      // If user is found, check role.
      if (foundUser.role == role) {
        return next()
      }

      res.status(401).json({ error: 'You are not authorized to view this content.' })
      return next('Unauthorized')
    })
  }
}

//=====================
// Register Middleware 
//=====================

const checkIfUserExists = (doc, req) => {
  const result = doc ? null : req
  if(result) {
    console.log('user not exits, we can continue');
    return result;
  } 
  console.log('throw error email');
  throw({error : {
    title: 'EmailAlreadyExists',
    message: 'That email address is already in use.'
  }})
}
const createUser = req => new User(req.body)
const saveUser = user => user.save()

exports.register = (req, res, next) => {  

  User.findOne({email: req.body.email})
    .then(doc => checkIfUserExists(doc, req))
    .then(createUser)
    .then(saveUser)
    .then(setUserInfo)    
    .then(user => {
      req.user = {
        user: user,
        token: 'JWT ' + generateToken(user)
      }            
      next()
    })
    .catch(err => {      
      res.status(400).json(err)
    })
}