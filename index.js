const app = require('./config/express')()

const authenticateApi = require('./api/users/controller')(app)
const installersApi = require('./api/installers/controller')(app)
const visitsApi = require('./api/installers/visits-calendar/controller')(app)
const clientsApi = require('./api/clients/controller')(app)
const chatsApi = require('./api/chats/controller')(app)
const installersFp = require('./api/installersFriopecas/controller')(app)

const port = process.env.PORT || 8080

const cronJob = require('./config/cron')
cronJob.start()

app.listen(port, () => {
    console.log('FrioPeças API is running')
})
