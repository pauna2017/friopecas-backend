const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const consign = require('consign')
const mongooseConfig = require('./mongoose')

module.exports = () => {    
    const app = express()

    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Headers", "Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Authorization, Access-Control-Request-Method, Access-Control-Request-Headers");
        res.setHeader("Access-Control-Allow-Credentials", "true");
        res.setHeader("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
        next();
    });
    app.use(morgan('tiny'))
    app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}))
    app.use(bodyParser.raw())
    app.use(bodyParser.json({limit: '50mb'}))
    app.use('/static', express.static('public'));
    mongooseConfig()

    return app
}
