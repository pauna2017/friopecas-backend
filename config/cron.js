const CronJob = require('cron').CronJob
const parser = require('xml2json')
const auth = require('btoa')('UNA:UNA@2017')
const rp = require('request-promise')
const InstallersFrioPecas = require('../api/installersFriopecas/model')

const onTick = () => {
    rp('http://wws.friopecas.com.br:8585/consulta?nome=profissional.buscar&tipo=xml&id=cad', {
        headers: {'Authorization': `Basic ${auth}`}
    })
        .then(response => {
            const data = new Date()
            console.log(`STARTED CRON JOB - ${data.toLocaleString()}` )
            const array = JSON.parse(parser.toJson(response)).cad.cadastro
            if(array.length) {
                InstallersFrioPecas.collection.remove({}, () => {
                    InstallersFrioPecas.collection.insert(
                        array, (err , docs) => {
                            if(err) {
                                console.log(err)
                            } else {
                                console.log(`INSERTS FINISH SUCCESFLY, NUMDOCS = ${array.length}`)
                            }
                    })
                })
            } else {
                console.log('ARRAY NO LENGTH')
            }
        })
        .catch(err => {
            console.log('Erro ao se communicar com a FrioPeças.')
        })
}

const job = new CronJob({
    cronTime: '00 00 04 * * 1-5',
    onTick: onTick,
    onComplete: null,
    start: true,
    timeZone: 'America/Sao_Paulo',
    runOnInit: true  
})
module.exports = job;
