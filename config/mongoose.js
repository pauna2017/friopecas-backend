const mongoose = require('mongoose')
const bluebird = require('bluebird')

module.exports = () => {
  // const dbURI = 'mongodb://adminfriopecas:pauna2017@ds019956.mlab.com:19956/friopecas'
  const dbURI = 'mongodb://adminfriopecas:pauna2017@ds131742.mlab.com:31742/dbfp'
  mongoose.connect(dbURI)
  mongoose.Promise = bluebird


  mongoose.connection.on('connected', 
    () => console.log('Mongoose default connection open to ' + dbURI))

  mongoose.connection.on('error',
    err => console.log('Mongoose default connection error: ' + err))

  mongoose.connection.on('disconnected', 
    () => console.log('Mongoose default connection disconnected'))

  mongoose.connection.on('open', 
   () => console.log('Mongoose default connection is open'))

  process.on('SIGINT', () => {
    mongoose.connection.close( () => {
      console.log('Mongoose default connection disconnected through app termination');
      process.exit(0)
    })
  }) 

}
